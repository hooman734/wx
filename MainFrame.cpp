#include "MainFrame.h"
#include <wx/wx.h>
#include <wx/spinctrl.h>


MainFrame::MainFrame(const wxString &title) : wxFrame(nullptr, wxID_ANY, title) {
    auto* panel = new wxPanel(this);
    auto* button = new wxButton(panel, wxID_ANY, "Add Item", wxPoint(350, 50), wxSize(100, 50), wxBU_EXACTFIT);
    auto* checkbox = new wxCheckBox(panel, wxID_ANY, "Check Box", wxPoint(550, 55), wxDefaultSize, wxCHK_3STATE | wxCHK_ALLOW_3RD_STATE_FOR_USER);
    auto* staticText = new wxStaticText(panel, wxID_ANY, "Hello World", wxPoint(120, 150));
    staticText->SetBackgroundColour(*wxLIGHT_GREY);
    auto* textCtrl = new wxTextCtrl(panel, wxID_ANY, "Input Something Here", wxPoint(500, 145), wxSize(200, -1));
    auto* slider = new wxSlider(panel, wxID_ANY, 77, 0, 100, wxPoint(100, 25), wxSize(200, -1), wxSL_VALUE_LABEL);
    auto* gauge1 = new wxGauge(panel, wxID_ANY, 100, wxPoint(500, 255), wxSize(200, -1), wxVERTICAL);
    auto* gauge2 = new wxGauge(panel, wxID_ANY, 100, wxPoint(500, 255), wxSize(200, -1), wxHORIZONTAL_HATCH);
    gauge1->SetValue(77);
    gauge2->SetValue(77);

    wxArrayString choices;
    choices.Add("Item C");
    choices.Add("Item B");
    choices.Add("Item A");
    choices.Add("Item D");

    auto* choice = new wxChoice(panel, wxID_ANY, wxPoint(200, 375), wxSize(100, -1), choices, wxCB_SORT);
    choice->Select(1);

    auto* spinCtrl = new wxSpinCtrl(panel, wxID_ANY, "", wxPoint(550, 375), wxSize(150, -1), wxSP_WRAP);

    auto* listBox = new wxListBox(panel, wxID_ANY, wxPoint(50, 400), wxDefaultSize, choices, wxLB_MULTIPLE);

    auto* radioBox = new wxRadioBox(panel, wxID_ANY, "RadioBox", wxPoint(485, 475), wxDefaultSize, choices,2, wxRA_SPECIFY_ROWS);

}
